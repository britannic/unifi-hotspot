#!/bin/bash
  
echo '>>> Starting Test Site unifi-hotspot Docker container'
docker run --name unifi-hotspot-tst -d \
-p 82:4545 \
-e MKT=https://api.fetchrev.com/v1/subscribers \
-e PASSWORD=HjilEwiJkg849 \
-e SECRET=secretString \
-e SITENAME=htl3o7z6 \
-e TOKEN=6e1a9ad4-121a-41ce-9afb-44f4b934ac30 \
-e URI=https://unifi.aws.helmrock.com:8443 \
-e USERNAME=hotspot \
britannic/unifi-hotspot:latest

echo '>>> Starting Beaverton unifi-hotspot Docker container'
docker run --name unifi-hotspot-bvx -d \
-p 83:4545 \
-e MKT=https://api.fetchrev.com/v1/subscribers \
-e PASSWORD=HjilEwiJkg849 \
-e SECRET=secretString \
-e SITENAME=f1u3q2kq \
-e TOKEN=afc9eafc-3c0b-466f-a2b9-b72067850239 \
-e URI=https://unifi.aws.helmrock.com:8443 \
-e USERNAME=hotspot \
britannic/unifi-hotspot:latest

echo '>>> Starting Portland unifi-hotspot Docker container'
docker run --name unifi-hotspot-pdx -d \
-p 84:4545 \
-e PASSWORD=HjilEwiJkg849 \
-e SECRET=secretString \
-e SITENAME=p25ldy3o \
-e TOKEN=c9ea1657-730a-4462-ab84-4e94cb2000f9 \
-e URI=https://unifi.aws.helmrock.com:8443 \
-e USERNAME=hotspot \
-e MKT=https://api.fetchrev.com/v1/subscribers \
britannic/unifi-hotspot:latest
