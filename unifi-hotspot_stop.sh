#!/usr/bin/env bash
set -e

echo '>>> Stopping all containers'
docker stop $(docker ps -aq)