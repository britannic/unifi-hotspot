// (C) Helm Rock Security Services, LLC

// GET request from UniFi Controller:
// "GET /guest/s/htl3o7z6/?id=34:36:3b:83:a1:36&ap=04:18:d6:20:0a:18&t=1546296969&url=http://captive.apple.com%2fhotspot-detect.html&ssid=Guest+Hotspot HTTP/1.0" 200 3071 "-" "CaptiveNetworkSupport-355.200.27 wispr"

var url = getMeta("uri");

// Get vars from meta tags
function getMeta(metaName) {
	const metas = document.getElementsByTagName("meta");
	for (let i = 0; i < metas.length; i++) {
		if (metas[i].getAttribute("name") === metaName) {
			return metas[i].getAttribute("content");
		}
	}
	// return ''
}

// // Button handler function to store the form data and AddSubscriber. 
// document.getElementById("FetchRevForm").onsubmit = function (e) {
// 	e.preventDefault(); // prevents default form submission process to allow AddSubscriber and validation
// 	AddNewSubscriber();
// 	login();
// };

// When the user clicks on div, open the popup
function popTOS() {
	var popup = document.getElementById("TOS");
	popup.classList.toggle("show");
}

function login() {
	var req = new XMLHttpRequest();
	req.open("POST", url + "/authorise", true);

	req.onreadystatechange = function () {
		if (req.readyState === 4 && req.status === 200) {
			console.log(JSON.parse(req.responseText));
		}
	};

	req.send();
}

// FetchRev API Access
function addSubscriber() {
	var data = {};
	data.firstname = document.getElementById("firstname").value;
	data.lastname = document.getElementById("lastname").value;
	data.email = document.getElementById("email").value;
	data.tel = document.getElementById("tel").value;
	var req = new XMLHttpRequest();
	var obj = JSON.stringify({
		email: data.email,
		phone_number: data.tel,
		first_name: data.firstname,
		last_name: data.lastname
	});

	req.open("POST", url + "/subscribers", true);
	req.setRequestHeader("Content-Type", "application/json");
	req.onreadystatechange = function () {
		if (req.readyState === 4 && req.status === 200) {
			console.log(JSON.parse(req.responseText));
		}
	};

	req.send(obj);
}

function submitForm(){
	addSubscriber();
	login();
}