#!/usr/bin/env bash
set -e

echo '>>> Stopping all containers'
docker stop $(docker ps -aq)

echo '>>> Removing all containers'
docker rm $(docker ps -aq)

echo '>>> Removing all images'
docker rmi $(docker images -q)

echo '>>> Getting latest unifi-hotspot Docker image'
docker pull britannic/unifi-hotspot

echo '>>> Starting new containers'
./unifi-hotspot_up