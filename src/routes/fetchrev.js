const express = require("express");
const authoriseFetchRev = express.Router();
const request = require("request-promise");
const bodyParser = require("body-parser");
authoriseFetchRev.use(bodyParser.urlencoded({
	extended: true
}));
authoriseFetchRev.use(bodyParser.json());

module.exports = function () {
	// debugger;
	authoriseFetchRev.route("/")
		.post(function (req, res, next) {
			let requestOptions = ({
				method: "POST",
				jar: true,
				json: true,
				strictSSL: false,
				headers: {
					"content-type": "application/json",
					"Authorization": "Bearer " + process.env.TOKEN
				},
				body: {
					"email": req.body.email,
					"phone_number": req.body.phone_number,
					"first_name": req.body.first_name,
					"last_name": req.body.last_name
				},
				uri: `${process.env.MKT}`
			});
			request(requestOptions)
				.then(loginResp => {
					console.log(loginResp);
					// res.status(200).send("Authenticated: connecting to WiFi")
					return request(requestOptions);
				})
				// .next("/authorise")
				// .then(auth => {
				// 	console.log(auth);
				// 	// res.redirect("/authorise");
				// 	next("/authorise");
				// })
				.catch(err => {
					res.status(400).send("unable to authenticate: " + err);
				});
		});
	return authoriseFetchRev;
};