const express = require("express");
const authoriseRouter = express.Router();
const request = require("request-promise");

module.exports = function () {
	authoriseRouter.route("/")
		.post(function (req, res) {
			let requestOptions = ({
				method: "POST",
				jar: true,
				json: true,
				strictSSL: false,
				uri: "",
				body: {}
			});
			console.log("Logging into UniFi Controller...");
			requestOptions.uri = `${process.env.URI}/api/login`;
			requestOptions.body = {
				username: process.env.USERNAME,
				password: process.env.PASSWORD
			};
			request(requestOptions)
				.then(loginResp => {
					console.log("Logged into UniFi Controller and processing client connection...");
					console.log(loginResp);
					requestOptions.uri = `${process.env.URI}/api/s/${process.env.SITENAME}/cmd/stamgr`;
					requestOptions.body = {
						ap_mac: req.session.ap_mac,
						cmd: "authorize-guest",
						mac: req.session.macAddr,
						minutes: process.env.MINUTES
					};
					return request(requestOptions);
				})
				.then(authResp => {
					console.log("Logging out of UniFi Controller...");
					console.log(authResp);
					requestOptions.uri = `${process.env.URI}/api/logout`;
					return request(requestOptions);
				})
				.then(logoutResp => {
					console.log(logoutResp);	
					console.log("Redirecting to http:s//myKingPins.com");
					res.redirect("https://mykingpins.com");
				})
				.catch(err => {
					console.log(err);
				});
		});
	return authoriseRouter;
};