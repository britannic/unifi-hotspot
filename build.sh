#!/bin/bash

version=$(cat VERSION)
echo "Building Node.js unifi-hotspot Docker image v${version}"
docker build -t britannic/unifi-hotspot .
docker tag britannic/unifi-hotspot britannic/unifi-hotspot:${version}
docker push britannic/unifi-hotspot
